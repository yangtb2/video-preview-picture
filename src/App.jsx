import React, { Component } from "react";
import PreviewBox from "./component/video-preview-box";

const data = {
  id: "ep80017",
  title: "【10月】四月是你的谎言 02",
  duration: 1379,
  link: "https://www.bilibili.com/bangumi/play/ep80017",
  pic: "/img/55b8ee8857a7f98c6d1b11e0ae8e0fab878aa909.jpg@206w_116h_1c_100q.webp",
  previewPic: {
    src: "/img/230907652.jpg@85q.webp",
    len_x: 10,
    len_y: 10
  },
  stat: {
    view: 8497153,
    like: 22548
  }
};

export default class App extends Component {
  constructor(props) {
    super(props);
    const div = document.getElementById("app");
    const { clientWidth: width, clientHeight: height } = div;
    this.state = {
      width,
      height
    };
  }

  componentDidMount() {
    const div = document.getElementById("app");
    window.addEventListener("resize", () => {
      const { clientWidth: width, clientHeight: height } = div;
      this.setState({ width, height });
    });
  }

  render() {
    const { width, height } = this.state;
    return <PreviewBox width={width} height={height} data={data} />;
  }
}
